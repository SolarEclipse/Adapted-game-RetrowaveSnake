## Retrowave Snake 未来复古贪吃蛇
<img src="intro/cover.jpg" width = "600" height = "430" alt="封面"/>

#### 介绍
未来复古风贪吃蛇游戏 (×)

未来复古风音乐播放器 (√)

#### 游戏特色
* 创新的贪吃蛇玩法，加入流星元素
* 80年代迷幻霓虹风格画面
* 精选数首retrowave/synthwave音乐

*__强烈建议进行游戏时打开声音！！！__*

#### 安装教程
1. 安装Python 3.7及以上版本
2. 安装pygame库
3. 运行retrowave-snake.py

#### 基本规则
* 通过WASD键或方向键控制你的蛇去吃随机生成的食物，但小心不要撞到墙壁或者你的身体。
* 随着存活时间的增加，你将伴随着获得一定的分数，但同时蛇的移动速度也将逐渐加快!
* 小心四面飞来的流星，当撞击到你的蛇的头部时，你将被判定死亡；当撞击到你的身体时，它将会把蛇身一分为二，你也将被扣除一定分数
* 快来在动感的节奏下，穿越到80年代开始舞动“蛇姿”吧！

#### 界面展示
<img src="intro/menu.gif" width = "300" height = "300" alt="菜单"/>
<img src="intro/gaming.gif" width = "300" height = "300" alt="游戏画面"/>
<img src="intro/end.gif" width = "300" height = "300" alt="结束画面"/>

#### 项目地址
https://gitee.com/SolarEclipse/Adapted-game-RetrowaveSnake

#### 游戏视频
https://www.bilibili.com/video/BV1KK411n7T1
